# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 06:52:56 2021

@author: megha
"""

# Detailed doc for mainpage.py
#
# @mainpage
#
# @section sec_port Portfolio Details
# This is my ME405 Portfolio. See individual modules for detals.
#
# Included modules are:
# * HW0x00 (\ref sec_lab0)
# * Lab0x01 (\ref sec_lab1)
# * Lab0x02 (\ref sec_lab1)
#
# @section sec_hw0 HWb0x00 Documentation
# * Source: file:///C:/Users/megha/Downloads/Untitled%20Diagram.html
# * Documentation: \ref Lab0
#
# @section sec_lab1 Lab0x01 Documentation
# * Source: https://bitbucket.org/MeghanDarcy/me405lab/src/master/lab1/darcylab1.py
# * Documentation: \ref Lab1
#
# @section sec_lab2 Lab0x02 Documentation
# * Source: https://bitbucket.org/MeghanDarcy/me405lab/src/master/lab2/darcylab2.py
# * Documentation: \ref Lab1
# @author Meghan Darcy
#
# @date 1/18/21
# @file mainpage.py
# Documentation for / use of mainpage.py
#
#