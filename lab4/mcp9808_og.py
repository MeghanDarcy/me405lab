# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 09:37:38 2021

@author: megha
"""

import time
import board
import busio
import adafruit_mcp9808
 
i2c_bus = busio.I2C(board.SCL, board.SDA)
 
# To initialise using the default address:
mcp = adafruit_mcp9808.MCP9808(i2c_bus)
 
# To initialise using a specified address:
# Necessary when, for example, connecting A0 to VDD to make address=0x19
# mcp = adafruit_mcp9808.MCP9808(i2c_bus, address=0x19)

## A method check() which verifies that the sensor is attached at the given bus address
# by checking that the value in the manufacturer ID register 
 
def celcius():
    tempC = mcp.temperature
    return tempC

def fahrenheit():
    tempF = mcp.temperature * 9 / 5 + 32
    return tempF
    

while True:
    tempC = mcp.temperature
    tempF = tempC * 9 / 5 + 32
    print("Temperature: {} C {} F ".format(tempC, tempF))
    time.sleep(2)