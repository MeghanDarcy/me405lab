# -*- coding: utf-8 -*-
"""
@file lab4.py
@brief This file acts to generate the plot of Temp V Time
@details This file takes the temperature_data_8hr csv and plots Board Temp, Temp in degC and Temp in Farinheit
@author Meghan Darcy
@date Feb. 07, 2021
"""

from matplotlib import pyplot
import csv


time_arr = []
tempB_arr = []
tempF_arr = []
tempC_arr = []

with open('temperature_data_8hr.csv', 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        time_arr.append(float(row[0]))
        tempB_arr.append(float(row[1]))
        tempC_arr.append(float(row[2]))
        tempF_arr.append(float(row[3]))

pyplot.plot(time_arr, tempB_arr,"o")
pyplot.plot(time_arr,tempC_arr,"o")
pyplot.plot(time_arr, tempF_arr,"o")
#pyplot.legend("Board Temperature","Yes", "No" )
pyplot.title ("Temperature vs. Time")
pyplot.xlabel("Time (min)")
pyplot.ylabel("Temperature")
pyplot.show()