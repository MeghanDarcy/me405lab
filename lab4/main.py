# -*- coding: utf-8 -*-
"""
@file main.py
@brief This file acts as the main to take temp data using mcp9808
@details This file uses the mcp9808 module and takes temperature readings 
         approximately every sixty seconds from the STM32 and from the
         MCP9808, and saves those readings in a CSV file on the microcontroller
@author Meghan Darcy 
@date Feb. 02, 2021
"""

import pyb
import utime
import micropython
import mcp9808
import array

#Established memory reserved for displaying error messages
micropython.alloc_emergency_exception_buf(200)

adc = pyb.ADCAll(12,0x70000)

def boardTemp():
    """
    @brief This function takes internal board temperature 
    @param This function uses adc.read_core_temp
    """
    temperatureB = adc.read_core_temp()
    return round(temperatureB,2)

try:
    time = array.array('H', [])
    tempB = array.array('f', [])
    tempC = array.array('H', [])
    tempF = array.array('H', [])
    sensor = mcp9808.mcp9808Class(1)
    start_time = utime.ticks_ms()
    print("The register should read 24 if the ID is correct: " + str(sensor.check()))
    for i in range(480):
        current_time = utime.ticks_ms()
        time_value = utime.ticks_diff(current_time, start_time)
        time_value1 = int(time_value)
        time.append(time_value1)
        current_celsius_value = int(sensor.celsius())
        current_fahrenheit_value = int(sensor.fahrenheit(current_celsius_value))
        tempC.append(current_celsius_value)
        x = boardTemp()
        tempB.append(x)
        tempF.append(current_fahrenheit_value)
        utime.sleep(60)
       
    with open ("temperature_data_8hr.csv","w") as csv_file:
        for idx in range(len(tempB)):
            csv_file.write("{:},{:},{:},{:}\r\n".format(time[idx],tempB[idx],tempC[idx],tempF[idx]))
   
except KeyboardInterrupt:
    print("{:},{:},{:},{:}\r\n".format(time,tempB,tempC,tempF))
    print('Program Closed')

'''
ampy --port COM3 put main.py main.py
ampy --port COM3 put mcp9808.py mcp9808.py
'''