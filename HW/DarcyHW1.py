# -*- coding: utf-8 -*-
"""
@filename: DarcyHW1.py
@author: Meghan Darcy
"""
def getChange(price, payment):
    """
    @ brief  Computes change for monetary transaction
    @ detail Computes change given set of bills/coins and returns 
             denominations
    @ param price - price of item as integer
    @ param payment - tuple of number of bills/coins used to pay
    @ return if funds are sufficent returns a tuple of bills/coins; if 
             insufficent returns none
    """
    coins = (1, 5, 10, 25, 100, 500, 1000, 2000)

    payment_int = 0
    for i in range(0, 7):
        payment_int += coins[i] * payment[i]

    change_int = payment_int - price

    if change_int < 0:
        return "NONE"

    change = [0,0,0,0,0,0,0,0]
    for j in reversed(range(0,7)):
        while (change_int >= coins[j]):
            change[j] += 1
            change_int -= coins[j]

    return tuple(change)
    






