# -*- coding: utf-8 -*-
# Detailed doc for mainpage.py
#
# @mainpage
#
# @section sec_port Portfolio Details
# This is my ME405 Portfolio. See individual modules for detals.
#
# Included modules are:
# * HW0x00 (\ref sec_lab0)
# * Lab0x01 (\ref sec_lab1)
# * Lab0x02 (\ref sec_lab2)
# * Lab0x04 (\ref sec_lab4)
# * Lab0x06 (\ref sec_lab6)
#
# @section sec_hw0 HWb0x00 Documentation
# * Source: file:///C:/Users/megha/Downloads/Untitled%20Diagram.html
# * Documentation: \ref Lab0
#
# @section sec_lab1 Lab0x01 Documentation
# * Source: https://bitbucket.org/MeghanDarcy/me405lab/src/master/lab1/darcylab1.py
# * Documentation: \ref Lab1
#
# @section sec_lab2 Lab0x02 Documentation
# * Source: https://bitbucket.org/MeghanDarcy/me405lab/src/master/lab2/darcylab2.py
# * Documentation: \ref Lab2
#
# @section sec_lab4 Lab0x04 Documentation
# * Source: https://bitbucket.org/MeghanDarcy/me405lab/src/master/lab4/
# * Documentation: \ref Lab4
#
# @section sec_lab6 Lab0x06 Documentation
# * Source: https://bitbucket.org/MeghanDarcy/me405lab/src/master/lab6/
# * Documentation: \ref Lab6
#
# @author Meghan Darcy
#
# @date 1/18/21
# @file mainpage.py
# Documentation for / use of mainpage.py
#
#
#