# -*- coding: utf-8 -*-
# Detailed doc for darcylab2.py
#
# @darcylab2
#
#
# @author Meghan Darcy
#
# @date 1/26/21
# @file darcylab2.py
# Documentation for / use of darcylab2.py
#
#

import random
import pyb
import micropython
import time
import urandom
import utime

print ('hi')
num_tries = 0;
start_time = 0;
bool_successful = False;
total_time = 0;
check=0;
check1=1;
u_time = 0;
reaction_time =0;

# init light
light = pyb.Pin (pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)

# init button
Pin13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.OUT_PP)

# Create emergency buffer to store errors that are thrown
micropython.alloc_emergency_exception_buf(200)


## Timer 2 prescalar and period
tim = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)
                
# define interrupt
isr_count = 0

def count_isr (Pin13):        # Create an interrupt service routine
    global isr_count
    isr_count += 1
    

    
## Interrupt init
extint = pyb.ExtInt (Pin13,                # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)

## main loop
while True:
    try:
        while isr_count == check:
            # wait random time and turn light on for one second
            random_time =int(random.choice([2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0])*1000)
            utime.sleep_ms(random_time)
            light.high()
            start_time = tim.counter()
            utime.sleep_ms(1000)
            light.low()
            check+= 1 
        while isr_count == check1:
            # once isr has been run 
            check1 +=1
            time_end = tim.counter()
            total_time += time_end-start_time
            print(total_time)
    except KeyboardInterrupt:
        # when i enter crt c no output is shown 
        # also causes a timeout waiting for EOF
        avg_time = float(total_time/isr_count)
        print("Your average time was %f seconds", avg_time)
        break