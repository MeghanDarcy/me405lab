# -*- coding: utf-8 -*-

"""@file darcylab1.py
Documentation for / use of darcylab1.py


@author Meghan Darcy
@date Jan. 18, 2021
"""

## @file mainpage.py
# Documentation for / use of mainpage.py
#
# Detailed doc for mainpage.py
#
# @mainpage
#
# @section sec_port Portfolio Details
# This is my ME405 Portfolio. See individual modules for detals.
#
# Included modules are:
# * HW0x00 (\ref sec_lab0)
# * Lab0x01 (\ref sec_lab1)
#
# @section sec_hw0 HWb0x00 Documentation
# * Source: file:///C:/Users/megha/Downloads/Untitled%20Diagram.html
# * Documentation: \ref Lab0
#
# @section sec_lab1 Lab0x01 Documentation
# * Source: https://bitbucket.org/MeghanDarcy/me405lab/src/master/lab1/darcylab1.py
# * Documentation: \ref Lab1
#
# @author Meghan Darcy
#
# @date 1/18/21

"""Initialize code to run FSM (not the init state). This code initializes the
 program, not the FSM
"""


def getChange(price, payment):
    
    ## @ brief  Computes change for monetary transaction
    #  @ detail Computes change given set of bills/coins and returns 
    #         denominations
    # @ param price - price of item as integer
    # @ param payment - tuple of number of bills/coins used to pay
    # @ return if funds are sufficent returns a tuple of bills/coins; if 
    #         insufficent returns none
    
    coins = (1, 5, 10, 25, 100, 500, 1000, 2000)

    payment_int = 0
    for i in range(0, 8):
        payment_int += coins[i] * payment[i]

    change_int = payment_int - price

    if change_int < 0:
        return "NONE"
    change = [0,0,0,0,0,0,0,0]
    for j in reversed(range(0,8)):
        while (change_int >= coins[j]):
            change[j] += 1
            change_int -= coins[j]

    return tuple(change)

def coinsToBal(payment):
    ## @ brief  Computes change list to float
    #  @ detail Computes change given set of bills/coins and returns 
    #         float balance
    #  @ param payment - tuple of number of bills/coins used to pay
    #  @ return float of payment
    
    coins = (1, 5, 10, 25, 100, 500, 1000, 2000)
    payment_int = 0
    for i in range(0, 8):
        payment_int += coins[i] * payment[i]
    return float(payment_int/100)

def printWelcome():
    
    ## @ brief  Prints Welcome Setup
    #  @ detail Prints welcome, coin options, and drink options 
    #  @ param n/a
    #  @ return Print statement of welcome messages 
    
    welcomeMessage = 'Vendtron Startup'
    coinOption = ' Enter 0=penny, 1=nickle, 2=dime, 3=quarter, 4=dollar, 5=$5, 6=$10, 7=$20'
    drinkOption = 'For button presses use E for eject, C for Cuke, P for Popsi, S for Spryte, and D for Dr. Pupper.'
    print(welcomeMessage)
    print(coinOption)
    print(drinkOption)

##
# @ brief  State describes position in State Transition Diagram
# @ detail State 0 = Init, State 1 = Hub, State 2 = Coins, State 3 = Drink
state = 0

while True:
    if state == 0:
        printWelcome()
        state = 1
        ##
        # @ brief  My String is user input
        # @ detail set in hub and reset after each pass
        my_string = ''
        ##
        # @ brief  My_Payment is list of coins/bills inserted by user
        # @ detail set in hub and reset only with eject
        my_payment = [0, 0, 0, 0, 0, 0, 0, 0]
        ##
        # @ brief  Price is cost of item
        # @ detail set in hub
        price = 0
        payment = 0
        print('Balance = $', coinsToBal(my_payment))
    
    elif state == 1: 
        ## STATE 1 = HUB
        #  Accepts user input and changes state based upon entry
        
        my_string = input('Please Press a Button:')
        if my_string == "E":
            state = 0
            print ('$',coinsToBal(my_payment),'Ejected')
        elif my_string == 'C':
            state = 3
            name = 'Cuke'
            my_price = 100
        elif my_string == 'P':
            state = 3
            name = 'Popsi'
            my_price = 120
        elif my_string == 'S':
            state = 3
            name = 'Spryte'
            my_price = 85
        elif my_string == 'D':
            state = 3
            name = 'Dr. Pupper'
            my_price = 110
        elif my_string == '1':
            state = 2
        elif my_string == '2':
            state = 2
        elif my_string == '3':
            state = 2
        elif my_string == '4':
            state = 2
        elif my_string == '5':
            state = 2
        elif my_string == '6':
            state = 2
        elif my_string == '7':
            state = 2
        elif my_string == '0':
            state = 2
        else:
            print('Error')
            
    elif state == 2:
        ## STATE 2 = Coins Entered 
        #  If coin is entered, add to balance and print new balance
        #  Return to Hub
        
        my_payment [int(my_string)] +=1
        print ('Balance = $',coinsToBal(my_payment))
        state = 1
        my_string = ''
    
    elif state == 3:
        ## STATE 3 = Drink Selected
        #  If Drink is selected, check balance, eject or error, return new balance
        #  Return to Hub
        if getChange(my_price,my_payment) == 'NONE':
            print('Insufficent Funds')
        else:
            print(name,'Ejected')
            my_payment = list(getChange(my_price,my_payment))
            print('Balance Remaining = $', coinsToBal(my_payment))
        state = 1
        my_string = ''
